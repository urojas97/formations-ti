# Intro Formation DOCKER Essential
- chroot (rootfs) --> changer le repertoire racine d'un processus
- namespaces --> Isolation des processus
- cgroups --> Gestion des ressources

Kernel Filesytem AUFS or OverlayFS

Driver overlay2 if kernel FS is OverlayFS

Un container contient ses propres libraries

Binaires:
- docker-engine <v13
- docker-ce >v13 

# Best Practices: 
- un container = un process
- ne pas utiliser l'utilisateur root dans le kernel

# Links:
- Plugin docker: https://docs.docker.com/engine/extend/
- OCI: https://www.opencontainers.org/

# Commandes: 

```bash
docker info
docker container ps
docker image prune
docker container prune
docker volume prune
docker system prune
docker container run nom_de_l_image (nom+tag)
docker container run ubuntu:xenial
docker container run -it ubuntu:xenial
```

(Detached container)
```bash
docker container run --name nom_instance -d ubuntu:xenial
```

(Attach -i permet de prendre en compte tes inputs clavier)

```bash
docker container run --name nom_instance -d -i ubuntu:xenial
docker container attach id_container
```
(**Attention**: sortir d'un container sans killer le processus principal )

    CTRL+P CTRL+Q

(Best practice)
```bash
docker container exec -ti ID_CONTENEUR /bin/bash
```

Pour Sortir:

    CTRL+C kill que le bash pas le processus principal


(stopper un conteneur)

Gracefull:
```bash
docker container stop
```

Force:
```bash
docker container kill
```

(Start conteneur)
```bash
docker container start ID_CONTENEUR
```

(supprimer conteneur)
```bash
docker container rm ID_CONTAINER
docker container run --rm -ti ubuntu:xenial /bin/bash
```
(inspection)
```bash
docker container inspect --format='{{.volmes}}' ID_CONTENEUR
--format='{{.HostConfig.VolumesFrom}}' ID_CONTENEUR
```
(Log)
```bash
docker container logs -f
```

(images)
```bash
docker image pull ubuntu:xenial
docker images ls -a (couche intermediaire)
docker images history
docker image rm ID_IMAGE (que si unitilisé)
docker search nom_image
docker image prune (que si unitilisé)
```

(creation images)
```bash
docker container commit ID_CONTAINER IMAGE_NAME:TAG 
    (non recommandé)
docker image build . 
    (input Dockerfile)
docker image tag image:TAG image:NEW_TAG 
    (creation de pointeur, les images son immuable)
```

(push image to registry)
```bash
docker login enthusiastic
```

Registry externe (Test en Local)
```bash
echo "$IP registry.form.ol" | sudo tee -a /etc/hosts
vi /etc/docker/daemon.conf
```
```yml
{
    "insecure-registries": [
        "registry.form.ol:5000"
    ]
}
```
Reload docker (systemctl)

Other example with install of the registry:
```bash
docker container run -d -p 5000:5000 registry:2.4
vi /etc/docker/daemon.conf
```
```yml
{
    "insecure-registries": ["registry.hostname:5000"]
}
```

Acces a la registry local:
```bash
docker image tag image:tag adresse.repos.local:port/username/image:tag
docker image push adress.repo.local/username/image:tag
```

# Questions:
Pattern cloud native
- Specification 
    - https://cnab.io/


# Support:
Version stable CE 4 mois

Version EE 1 an


# Architecture Docker

```dot
digraph ArchitectureDocker {
    rankdir=LR;
    "Client Docker" -> "api rest (standard OCI)"
    "api rest (standard OCI)" -> Daemon
}
```
    Via socket UNIX (/var/run/docker.sock) ou TCP (certificats)

Daemon docker tourne en root

**Registry**

- Docker Hub (pull push) Saas
    - Public gratuit
    - Private payant
- Private Registry
    - Registre miroir
- Docker Store 
    - docker plugin




# Installation 
```bash
sudo yum install -y yum-utils device-mapper-persistent-data lvm2
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install docker-ce -y
sudo systemctl start docker
sudo systemctl enable docker
sudo usermod -aG docker $USER
sudo yum -y install bash-completion
sudo curl https://raw.githubusercontent.com/docker/docker-ce/master/components/cli/contrib/completion/bash/docker -o /etc/bash_completion.d/docker.sh
exec bash
```

# Installation docker swarm

## Node1 Master
```bash
sudo yum install -y yum-utils device-mapper-persistent-data lvm2
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install docker-ce -y
sudo systemctl start docker
sudo systemctl enable docker
sudo usermod -aG docker $USER
sudo yum -y install bash-completion
sudo curl https://raw.githubusercontent.com/docker/docker-ce/master/components/cli/contrib/completion/bash/docker -o /etc/bash_completion.d/docker.sh
exec bash
docker swarm init
```
Output:

    sudo docker swarm join --token SWMTKN-1-6958bs4jrp653ggyiuuv49kcdsogd3chrytjiyhww8r4o3odig-1vjy0aw1c30mxcxalqmmvnnxm 54.39.189.109:2377


## Node2 et 3 Slave

Idem node 1 sauf swarm init

Lancer l'output du node1:
```bash
sudo docker swarm join --token SWMTKN-1-6958bs4jrp653ggyiuuv49kcdsogd3chrytjiyhww8r4o3odig-1vjy0aw1c30mxcxalqmmvnnxm 54.39.189.109:2377
```

Si besoin de recuperer un token manager ou worker:
```bash
docker swarm join-token worker
docker swarm join-token manager
```
    Pour avoir plusieurs manager si le node n'est pas un worker

```bash
docker node ls
```

**Les managers**

- Surveille le cluster
- deploiement des services
- commandes
    - >docker node promote
    - >docker node demote
- dispatcher

Config nb manager pour Haute dispo:
| Nb Manager | Fault Tolerance | Env |
|:----------:|:-------------:|:------:|
| 1 |  0 | dev |
| 3 |    1   |   petit |
| 5 | 2 |   moyen |
| 7 | 3 |   large |
| N | (N-1)/2 |   NA |



# Créer un service
```bash
docker service ps
docker services logs
docker service create alpine ping 8.8.8.8
docker service ls
docker service ps
docker service logs
docker service create --replicas 3 nginx
docker service update  --replicas=3
```

Expose service:
- routing mesh


# Integration continue

- Gitlab
    - runner

- Jenkins
    - slave

# Gestion des secret

Internal distributed store (Action on Master nodes)

?least privilege?
```bash
echo "password" | docker secret create my-secret -
echo "mysql-pass" > ./mysql-password.txt
docker secret create mysql-password ./mysql-password.txt
docker secret ls
docker secret inspect $secret
docker network create --driver overlay wordpress-network
docker service create --name wordpress-db \
    --network wordpress-network \
    --secret mysql-password \
    -e MYSQL_ROOT_PASSWORD_FILE=/run/secrets/mysql-password \
    mysql:5.7
```

"--secret mysql-password" copie le secret en clair dans le répertoire /run/secrets/ du container

# Docker behind a proxy

1. directement dans le daemon docker

2. lors du build
```bash
docker image build -f /path/dockerfile -build-arg http_proxy=http://proxy:3128
```

# Moby project
https://mobyproject.org/



# Network

- Mode host
- Mode container
- Mode bridge (docker0) @IP privée (NAT) (default)
```bash
docker network create my_bridge
docker container run --network my_bridge -d nginx
docker container run --network none
docker container run -it --network host -d nginx
ss -ltpn
```
version libnetwork >1.9
    
    Ajout d'une interface bridge a chaux

```bash
docker network connect
```

Important: Le dns interne a Docker ne fonctionne pas si on utilise docker0, il ne fonctionne que dans les custom network (network create)

(expose)
```bash
docker container run -p 8080:80 -d
```
    -p 89/udp
```bash
docker port id_container port
```
    -p port_conteneur (choix aleatoire sur l'hote)
    -P (bind automatiquement les port exposé dans le conteneur)
    -p @ip:port_conteneur1-port_conteneur2
    -p @ip:port_hote:port_conteneur

# Gestion des données
```bash
docker volume create NOM_volume
docker volume inspect Nom_volume
```    
mountpoint

```bash
docker container run -v volume1:/var/lib/mysql -v volume2:/var/log:rw -v volume3:/data:ro
```

- On peut binder un fichier ou un dossier de l'hote
- On peut partager un socket unix (DinD)
- Lorsque le volume est vide, lors du montage, les données sont copiées
- On ne peut pas rajouter un volume a la volée


# Dockerfile

## Directives

Installation de paquet


Copier des fichiers
```dockerfile
    COPY
    ADD extraction automatique d'archive, URL
```
Variable d'environnements
```dockerfile
    ENV pendant la construction et le RUN de l,image
    ARG que pendant la construction de l'image
```
Changer de repertoire pour lancer une commande par example
```dockerfile 
    WORKDIR
        -> Impact RUN, ADD, COPY, CMD, ENTRYPOINT
```
Utiliser un user
```dockerfile
    USER
        Doit etre créé au prealable 
```
C'est la premiere instruction qui va etre lancer
```dockerfile
    ONBUILD
```
Changer de shell
```dockerfile 
    SHELL
        -> Impact CMD, ENTRYPOINT
```
Executer une commande
```dockerfile 
    CMD
        -> peut etre surchargé, une seul directive 
    ENTRYPOINT
        -> exemple:
            ENTRYPOINT : ping
            CMD : 127.0.0.1
        -> entrypoint.sh a la fin ($@ lance la commande contenu dans CMD)
        -> Une seul directive
        ->Syntaxe["cmd","option","option"]
```
Gestion des volumes
```dockerfile 
    VOLUME
        renseignement por les autres utilisateurs
```
Binding des ports reseaux
```dockerfile
    EXPOSE
```
Healthcheck
```dockerfile
    HEALTHCHECK
        --interval
        ...
```
Renseignement du dockerfile
```dockerfile
    MAINTENER (deprecated)
    LABEL (key value)
```
Bonne pratiques:
```dockerfile
    RUN cmd && cmd
    EXPOSE , 
    VOLUME , 
    ENTRYPOINT , 
    CMD a la fin
```
# Multistage
```dot
digraph notebook {
    rankdir=LR
    image1 -> Maven;
    Maven -> Compil;
    Compil -> ".jar";
}
```

```dot
digraph notebook {
    rankdir=LR
    image2 -> "COPY --from=image1 .jar";
}
```


# Supervisord

Le software pour gestion des logs et schedule des process en l'interieur du container

    supervisord.conf
lancé en root
supervisord est le process parent
    
    "systemctl plus light"

# Vulnerabilités des images

Scanner des images

- aqua (gratuit)

- Certification des images (Notary)
    - Revocation possible

Certaine registry son incorporé dans d'autre logiciel comme Gitlab


# Lancement automatique des conteneurs

1. Via docker container run
```bash
docker container run --restart unless-stopped redis
```
- no
    - ne pas relancer
- on-failure[:max-retries]
    - si code de sortie 0
- always
- unless-stopped
    - sauf si le container est arreté manuellement ou autre

2. via fichier de service systemd
    Creer un service pour starter les container quand le docker daemon démarre

# Gestion des ressources
```bash
docker container stats
docker container top
```

Option au run:

    -m 512m
    --memory-swap=128m
        -> Activation au demarrage de l'hote:
            cgroup_enable=memory swapaccount=1

Poids CPU container (default 1024)
```bash
docker container run --cpu-shares 512 --cpu-shares 2048
```

Choisir CPU
    
    --cpuset-cpu="0"
    --cpuset-cpu="1-3"
        --> Activation au demarrage de l'hote:
            cgroup_enable=cpu (TBD)

# Gestion de la sauvegarde
- Seul les volumes sont sauvegardés
- Les images il faut utiliser une registry

COMMANDE:
```bash
docker container export
docker container import
docker image save
docker image load
```

Exemple de Backup:
    
    Cronjob:
    Lancer un container de backup avec les volumes en Read-only du serveur a backuper + un volume de backup, qui tar ce qui est contenu dans les volumes en readonly sur le volume de backup


# Gestion des logs

Drivers:
    
- json-file
    - default, docker container logs
- syslog
    - les logs des container vont dans le fichier syslog de l'hote
- journald
    - les logs des container vont dans journald
- gelf
    - vers graylog
- fluentd
- none

# Bonnes pratiques

- Lancer les processus en non-root
- Isolastion physique et network par environnement
- Limit cpu et memoire
- Mettre le systeme de fichier en Read-only
- Utiliser que des images officiels
- Pas de variable d'environnment et de mot de passe dans les builds
- Utiliser une gestion des secrets
- profil seccomp (permet d'appliquer des profils de securite sur les container)
    - --security-opt=
- Docker content trust (certification/revocation des images)

# Docker compose

- Installation de docker compose
- Fichier de description yaml -> docker-compose.yml
- La version 2.2 doit etre specifié
- Un docker compose egal une stack applicative avec son propre reseau
- tout les containers tournee en détaché
- on utilise pas it ou -d dans le fichier

Directives:

Permet de spécifier l'ordre des containers
    
    depends_on
```bash
docker compose up
docker compose down
```

```yml
version: '2'

service:
    zabbix-db:
        image:
        volumes:
        networks:
    
    zabbix-server:
        image:
        depends_on:
            - zabbix-db
        ports:
            - 80:80
        volumes:
            - /etc/localtime:/etc/localtime:ro

    image-to-build:
        build:
            context: ./dir
            dockerfile: dockerfile-monimage.yml
        image: mon image
        environment:
            DB_HOST:db
        ports:
volumes:
    zabbix-db-storage
        driver: local
    backups:
        driver: local

networks:
```
```bash
docker-compose build
docker-compose up
docker-compose up -d
docker-compose down
docker-compose rm
```

Démarrer un seul service du docker-compose
```bash
docker-compose stop wordpress
docker-compose start wordpress
```

La version 3 prend en compte swarm (stack deploy see below)

Fonctionnalités avancées:
- Ordonnancement des services
    - depends_on
- Scaling de service
    - docker-compose scale web=2



# TP 1 Run container
```bash
docker container run --rm -it debian:wheezy cat /etc/debian_version
docker container run --rm -it debian:jessie cat /etc/debian_version
docker container run --rm -it ubuntu:trusty cat /etc/debian_version
docker container run --rm -it ubuntu:xenial cat /etc/debian_version 
docker container run -it debian:jessie
docker container ls
docker container start -i ID_CONTAINER
docker run -e ZZZ="sommeil" alpine:latest env
docker container run -d -ti debian:wheezy /bin/bash -c "while /bin/true; do echo 'Je boucle encore et encore...'; sleep 1; done"
docker container logs
docker container logs -f
docker container attach
```
    CTRL+P CTRL+Q
```bash
docker container kill
docker container rm
```

# TP 2 Push image to registry
```bash
docker container run -it debian:jessie
apt-get update && apt-get install iputils-ping net-tools procps vim
```  
    CTRL+P CTRL+Q
```bash
docker container ls
docker container commit abbe2afaaa7c enthusiastic/formation:v1
docker login
docker image push enthusiastic/formation:v1
```

# TP 3 Network creation
```bash
docker network create my_bridge
docker container run -it --network my_bridge -d --name test1 debian:8
docker exec -it test1 /bin/bash
```

# TP 4 Expose port
```bash
docker container run -p 8080:80 -d nginx
```

# TP 5 Install Wordpress manually
```bash
docker network create net_public
docker container run -it --network net_public -d --name wordpress debian:8
docker exec -it wordpress /bin/bash
apt-get update
apt-get install wget apache2 libapache2-mod-php5 php5-mysql -y
wget --no-check-certificate https://wordpress.org/latest.tar.gz
tar xf latest.tar.gz
mv wordpress /var/www/html
chown -R www-data:www-data /var/www/html
docker container commit wordpress enthusiastic/wordpress:v1
docker image push enthusiastic/wordpress:v1
docker container run -it --network net_public -p 8080:80 -d --name prd_wordpress enthusiastic/wordpress:v1 apachectl -D FOREGROUND
```

Web browser: http://54.39.189.109:8080/wordpress
```bash
docker container run -it -e MYSQL_ROOT_PASSWORD=mysecretpw -e MYSQL_DATABASE=wordpress -e MYSQL_USER=wordpress -e MYSQL_PASSWORD=wordpress --network net_public -d --name mysqlv2 mysql:5.6
docker container commit mysql enthusiastic/mysql:v1
docker image push enthusiastic/mysql:v1
docker container run -it --network net_public -d --name prd_mysql enthusiastic/mysql:v2
```

# TP 6 Create volume
```bash
docker volume create nginx_share
docker container run -p 8090:80 -d -v nginx_share:/usr/share/nginx/html/ nginx:1.15
sudo vi /var/lib/docker/volumes/nginx_share/_data/index.html
```

# TP 7 Install project 2048 from git
```bash
sudo yum install git
git clone https://github.com/ObjectifLibre/2048
docker container run -p 8010:80 -d -v /home/centos/2048:/usr/share/nginx/html/ nginx:1.15
```

# TP 8 First dockerfile

```bash
sudo tee Dockerfile <<-EOF
```
```dockerfile
FROM debian:8

RUN apt-get update && apt-get install -y cowsay
ENV PATH $PATH:/usr/games
ENTRYPOINT ["cowsay"]
CMD ["Jojo, mon héros"]
```
```bash
EOF
```
```bash
docker image build -t enthusiastic/cowsay:v1 .
docker image push enthusiastic/cowsay:v1
docker container run -it --name cowsay enthusiastic/cowsay:v1
```

# TP 9 Wordpress in dockerfile

## Wordpress

```bash
mkdir wordpress && cd wordpress
sudo tee Dockerfile <<-EOF
```
```dockerfile
FROM debian:8

RUN apt-get update && apt-get install wget apache2 libapache2-mod-php5 php5-mysql -y
WORKDIR /var/www/html/
ADD https://wordpress.org/latest.tar.gz .
RUN tar xf latest.tar.gz && rm -f latest.tar.gz && chown -R www-data:www-data /var/www/html

EXPOSE 80
ENTRYPOINT ["apachectl"]
CMD ["-D","FOREGROUND"]
```
```bash
EOF
```
```bash
docker image build -t enthusiastic/wordpress_dockerfile:v6 .
docker image push enthusiastic/wordpress_dockerfile:v6
docker container run -it --network net_public -p 9080:80 -d --name prd_wordpress enthusiastic/wordpress_dockerfile:v6
```
Persistance des données /var/www/html/
```bash
docker volume create wordpress_html
docker container run -it --network net_public -p 9009:80 -d -v wordpress_html:/var/www/html/ --name prd_wp enthusiastic/wordpress_dockerfile:v6
```

## MYSQL

```bash
mkdir ~/mysql_wp && cd ~/mysql_wp
sudo tee Dockerfile <<-EOF
```
```dockerfile
FROM mysql:5.6

ENV MYSQL_ROOT_PASSWORD=mysecretpw
ENV MYSQL_DATABASE=wordpress
ENV MYSQL_USER=wordpress
ENV MYSQL_PASSWORD=wordpress

EXPOSE 3306
```
```bash
EOF
```
```bash
docker image build -t enthusiastic/mysql_wp:v1 .
docker image push enthusiastic/mysql_wp:v1
docker container run -it --network net_public -d --name mysql_wp enthusiastic/mysql_wp:v1
docker container run -it -e MYSQL_ROOT_PASSWORD=mysecretpw -e MYSQL_DATABASE=wordpress -e MYSQL_USER=wordpress -e MYSQL_PASSWORD=wordpress --network net_public -d --name mysql_wp mysql:5.6
```
Persistance des données /var/lib/mysql/
```bash
docker volume create wordpress_mysql
docker container run -it --network net_public -d -v wordpress_mysql:/var/lib/mysql/ --name prd_wp_mysql enthusiastic/mysql_wp:v1
```
## Upgrade SQL

```bash
cd ~/mysql_wp
sudo tee Dockerfile <<-EOF
```
```dockerfile
FROM mysql:5.7

ENV MYSQL_ROOT_PASSWORD=mysecretpw
ENV MYSQL_DATABASE=wordpress
ENV MYSQL_USER=wordpress
ENV MYSQL_PASSWORD=wordpress

EXPOSE 3306
```
```bash
EOF
```

```bash
docker image build -t enthusiastic/mysql_wp:v2 .
docker image push enthusiastic/mysql_wp:v2
docker container rm -f prd_wp_mysql; docker container run -it --network net_public -d -v wordpress_mysql:/var/lib/mysql/ --name prd_wp_mysql enthusiastic/mysql_wp:v2
```

# TP 10 First docker-compose file
```bash
sudo curl -L https://github.com/docker/compose/releases/download/1.23.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose version
```

## Creation Dockerfile

```dockerfile
#Définition de l'image de base
FROM debian:8

ADD https://wordpress.org/latest.tar.gz /tmp/wordpress.tar.gz

#On reprend les commandes en faisant attention à ce qu'elles puissent
#s'exécuter automatiquement
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        apache2 \
        libapache2-mod-php5 \
        php5-mysql && \
    tar xf /tmp/wordpress.tar.gz -C /var/www/html && \
    chown -R www-data:www-data /var/www/html && \
    rm -rf /var/lib/apt/lists/*

RUN ln -sf /dev/stdout /var/log/apache2/access.log && \
    ln -sf /dev/stderr /var/log/apache2/error.log

#Définir la commande à exécuter, elle ne doit pas forker
CMD ["apache2ctl", "-DFOREGROUND"]

EXPOSE 80

VOLUME /var/www/html
WORKDIR /var/www/html
```

## Creation du fichier docker-compose.yml

```yml
version: "2.2"

services:
  mysql:
      image: mysql:5.6
      restart: always
      volumes:
        - mysql-vol:/var/lib/mysql
      networks:
        wp-network:
          ipv4_address: 192.170.0.3
      environment:
        MYSQL_ROOT_PASSWORD: toto
        MYSQL_DATABASE: wordpress
        MYSQL_USER: wordpress
        MYSQL_PASSWORD: wordpress

  wordpress:
      build:
        context: .
        dockerfile: Dockerfile
      image: my-wordpress:0.1
      restart: always
      ports:
        - 8080:80
      networks:
        wp-network:
          ipv4_address: 192.170.0.2
#      mem_limit: 300m
#      mem_reservation: 100m
#      cpu_shares: 50
      volumes:
        - wp-vol:/var/www/html
      logging:
        driver: syslog
        options:
          syslog-address: "udp://127.0.0.1:514"
      depends_on:
      - mysql


#  rsyslog:
#      image: objectiflibre/syslog:latest
#      ports:
#      - "514:514/udp"
#      networks:
#        wp-network:
#          ipv4_address: 192.170.0.4

networks:
  wp-network:
    ipam:
      driver: default
      config:
      -  subnet: 192.170.0.0/16

volumes:
  mysql-vol:
  wp-vol:
```

Lien source app.py et autre:

https://github.com/Fantomatic/trainingDocker.git

>docker-compose up -d


# TP 11 (Bonus) Traeffic 

```bash
mkdir app-python && cd app-python; cp ../app.py .; cp ../requirements.txt .
sudo tee Dockerfile <<-EOF
```
```dockerfile
FROM python:3.4-alpine

RUN mkdir /code
WORKDIR /code

ADD app.py .
ADD requirements.txt .
RUN pip install -r requirements.txt

EXPOSE 5000
ENTRYPOINT ["python"]
CMD ["app.py"]
```
```bash
EOF
```

```bash
sudo tee docker-compose.yml <<-EOF
```
```yml
version: '2'

services:

  #Application python
    web:
        build:
            context: .
            dockerfile: Dockerfile
        image: web-app-py:0.1
        #restart: always
        #ports:
        #   - 5000
        networks:
            app-network:
        volumes:
            - app-code:/code
        #Option utiliser par le load balancer
        labels:
            #- traefik.enable=true
            #- "traefik.docker.network=app-network"
            #- "traefik.port=5000"
            - traefik.frontend.rule=Host:whoami.docker.localhost
        depends_on:
        - redis

    redis:
        image: redis
        restart: always
        volumes:
            - redis-vol:/usr/local/etc/redis/
        networks:
            app-network:

    lb:
        image: traefik:alpine
        command: --api --docker
        ports:
            - 80:80
            - 8080:8080
        volumes:
            #- /docker/traefik:/etc/traefik
            - /var/run/docker.sock:/var/run/docker.sock
        networks:
            - app-network

networks:
  app-network:

volumes:
  redis-vol:
  app-code:
```
```bash
EOF
```

```bash
docker-compose up -d
docker-compose scale web=2 deprecated
docker-compose up -d --scale web=2
```

Pour supprimer tout les  containers (en lab seulement :))
```bash
for i in `docker container ls | grep -v CONTAINER | awk '{print $1}'` ; do docker container rm -f $i ;done
```

# TP 12 Docker Swarm

```bash
docker services logs
docker service create alpine ping 8.8.8.8
docker service ls
docker service ps
docker service logs
docker service create \
    --name who-am-i \
    --publish 8000:8000 \
    --replicas 3 \
    --constraint node.hostname==csauthier-formation-docker-4 \
    training/whoami:latest
docker service create \
    --name who-am-i \
    --publish 8000:8000 \
    --replicas 3 \
    training/whoami:latest
curl 127.0.0.1:8000
```

```bash
sudo systemctl enable firewalld.service
sudo systemctl start firewalld.service
sudo firewall-cmd --zone=public --permanent --add-port=2377/tcp
sudo firewall-cmd --zone=public --permanent --add-port=7946/tcp
sudo firewall-cmd --zone=public --permanent --add-port=7946/udp
sudo firewall-cmd --zone=public --permanent --add-port=4789/udp
sudo systemctl reload firewalld
```


```bash
docker service create --name nginx \
    --publish 8080:80 \
    --constraint node.role==worker \
    nginx
watch docker service ps nginx
docker stack deploy -c docker
docker node ls
sudo systemctl reload firewalld
```

# TP 13 registry (Failed with traeffik)

Docker-compose file: 

```bash
sudo tee docker-compose.yml <<-EOF
```
```yml
version: '3'

services:
    registry:
        image: registry:2.4
        networks:
            registry-network:
        volumes:
            - registry:/var/lib/registry
        #Option utiliser par le load balancer
        labels:
            - traefik.frontend.rule=Host:registry.docker.localhost

    traefix:
        image: traefik:alpine
        command: --api --docker
        ports:
            - 5000:5000
        volumes:
            #- /docker/traefik:/etc/traefik
            - /var/run/docker.sock:/var/run/docker.sock
        networks:
            - registry-network

networks:
  registry-network:

volumes:
  registry:
```
```bash
EOF
```
File to modify to add external registries:
    
>/etc/docker/daemon.json

```yml
{
    "insecure-registries": ["registry.docker.localhost:5000"]
}
```

```bash
docker info
systemctl reload docker
curl -x GET http://
```

mode proxy:
```yml
{
    "registry-mirrors": ["https://docker-hub.objectiflibre"]
}
```