# README #

Repo de partage de prise de notes en formation.

Si vous voulez r�aliser de la documetation en markdown j'ai mis un template d'exemple pour une prise en main rapide:
[Exemple_markdown](./Template_Markdown.md)

# Recommendations

Clone and use vscode to read the formation notes (bitbucket doesn't understand digraph) with this extension to preview markdown file:

	Name: Markdown Preview Enhanced
	Id: shd101wyy.markdown-preview-enhanced
	Description: Markdown Preview Enhanced ported to vscode
	Version: 0.5.0
	Publisher: Yiyi Wang
	VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=shd101wyy.markdown-preview-enhanced


