# Formation Kubernetes 2019
## Introduction

CNCF

Kubernetes moteur CRI Common Runtime Interface
image OCI

Control Plane
    apiserver
        heberge sur le master
    controller-manger
    scheduker
Data plane
    kubelet
    kube-proxy
        kubectl
        webUI
            plus limité que kubectl
    plugin reseau 
        ex: calico

Loadbalancer externe ou interne

## Premiers pas

Pods Deployment Service Ingress

Pods (n container + shared stockage + shared loopback)

Volumes
    read-write many
    read-write once

Stockage
    Local
        Host pass
    Driver Stockage

Replicasets
    Duplicat de pods
    
Deployment
    Gestion de version

Services
    Service reseau
        Loadbalancer
        NodePort NAT de base (scope hosts)
        ClusterIP Dispo sur toute les nodes
        Ingress s'interface devant les services


```bash
#!/bin/bash

sudo apt-get update && sudo apt-get -y install curl
curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.16.3/bin/linux/amd64/kubectl
chmod +x kubectl
sudo mv kubectl /usr/local/bin

# On pensera à ajouter l'auto-completion dans son bashrc
echo "source <(kubectl completion bash)" >> ~/.bashrc


#!/bin/bash
sudo apt-get update && sudo apt-get install -y curl virtualbox jq
curl -LO https://storage.googleapis.com/minikube/releases/v1.5.2/minikube-linux-amd64 \
        && sudo install minikube-linux-amd64 /usr/local/bin/minikube
minikube start --memory 8192 --vm-driver=virtualbox --kubernetes-version v1.16.3

minikube ssh
sudo -i
sysctl -w vm.max_map_count=262144


minikube dashboard
kubectl proxy --disable-filter=true --address='0.0.0.0' &
```

http://54.38.38.206:8001/

http://54.38.38.206:8001/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/

Creation d'un service nginx via l'interface graphique
kubectl expose deployment nginx --type=NodePort --name=servicenginx --port 80


## Help
kubectl help
kubectl config -h

## Configure kubectl
kubectl config get-contexts
kubectl config set-cluster cluster-demo --embed-certs --server=https://cluster-demo.formation.corp:6443 --certificate-authority=/tmp/ca-kubernetes.pem

sudo tee deployment_nginx2.yml <<-EOF
apiVersion: apps/v1 # for versions before 1.9.0 use apps/v1beta2
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  selector:
    matchLabels:
      app: nginx2
  replicas: 3 # tells deployment to run 2 pods matching the template
  template:
    metadata:
      labels:
        app: nginx2
    spec:
      containers:
      - name: nginx2
        image: nginx:1.7.9
        ports:
        - containerPort: 80
EOF

kubectl expose deployment nginx-deployment --type=NodePort --name=service-nginx2
kubectl get endpoints
kubectl get services
curl ipendpoint:portservices

ssh -L 127.0.0.1:80:192.168.99.100:80 54.38.38.206

ssh -R 192.168.99.100:30270:localhost:80 ubuntu@54.38.38.206

ssh -R 30270:192.168.99.100:30270 ubuntu@54.38.38.206

sudo sysctl net.ipv4.ip_forward=1

iptables -t nat -A PREROUTING -p tcp -d 54.38.38.206 --dport 30270 -j DNAT --to-destination 192.168.99.100:30270

iptables -t nat -A POSTROUTING -j MASQUERADE


http://54.38.38.206:30270/



# TP2 Partie 1

kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-beta6/aio/deploy/recommended.yaml
kubectl proxy
kubectl describe secret kubernetes-dashboard -n kubernetes-dashboard

http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/



Kubernetes
OK


## Ingress
un ingress par namespace
Ingress TLS
    certmanager


kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/cloud-generic.yaml

https://kubernetes.github.io/ingress-nginx/deploy/#docker-for-mac


helm repo add stable https://kubernetes-charts.storage.googleapis.com


## Storage

PVC

Storage class

Empty dir
    Memoire
    A surveiller

HostPath
    path sur disque
    Attention au UID

kubectl get storageclasses
kubectl describe storageclasses hostpath

PersistentVolumeClaim:
    ClaimName:

accessModes:
    - ReadWriteOnce


nfs 
    necessite nfs-client sur chaque node


## Secret

ConfigMaps
    gestion des fichiers de configuration
        les fichiers peuvent etre monter directement depuis la configmpa

Secret
    encoder en base64
    
    env
        valuefrom

## Advanced

init container

hooks
    postStart
    Prestop
        
        http
    lifecycle:

    kubectl logs 2048-deploy-76ff48d78c-lx9t2 -c initcontainer2048 -n formation

# Daemonset

curl -XPOST "http://kubernetes.docker.internal:32001/_search" -d'   {     "query": {       "match_all": {}     }   }' | jq .

kubectl run demodate --image=ubuntu --replicas=1 --command -- /bin/bash -c 'while true; do echo "date: $(date)"; sleep 2; done'

## Operator
prometheus-operator
    values.yaml


    kubectl get all -A -l release=prom-op

    CRD
        kubectl get servicemonitor -A
    Custom Ressources Definitions

operator-sdk new maridb-operator --type=helm --helm-chart stable/mariadb

prometheus 

cadvisor

kustomize

HPA


IAM
RBAC
    Kind: Role
    rules
        apiGroups
            resources
            verbs (action)


vault
- Pod
    serviceAccount
    annotation
        vault.
            utilse des credentials (servicesAccountName)
    --set vault:

cubbyhole/ temporaire

KV= key/value
v2 versioning de secret
ACl policies
wrap fichier json


Utilisation d'un serveur KMS



# Lab Istio/K3d TCP

## Build image ansibletools

Dockerfile ~/workspace/ansible/Dockerfile

docker image build -t enthusiastic/ansible-tools:0.3 .

docker image push enthusiastic/ansible-tools:0.3

```dockerfile
#Définition de l'image de base
FROM rastasheep/ubuntu-sshd:18.04

#On reprend les commandes en faisant attention à ce qu'elles puissent
#s'exécuter automatiquement
RUN apt update -y && \
    export DEBIAN_FRONTEND=noninteractive && \
    apt install apt-utils -y && \
    apt install debconf-utils -y && \
    apt install python-pip -y && \
    apt install graphviz -y && \
    pip install ansible==2.8.6 && \
    pip install graphviz && \
    pip install colour && \
    pip install lxml && \
    pip install packaging && \ 
    pip install ansible-playbook-grapher && \
    pip install ansible-inventory-grapher && \
    apt install iputils-ping -y && \
    apt install iproute2 -y && \
    apt install git -y && \
    apt install libsasl2-dev python-dev libldap2-dev libssl-dev -y && \
    pip install dpath ldap3 pyDes python-ldap dnspython && \
    apt install net-tools -y && \
    apt install vim -y 

#Définir la commande à exécuter, elle ne doit pas forker
CMD ["/usr/sbin/sshd", "-D", "-p", "220"]

EXPOSE 220
```



## Build image golangtools

Dockerfile ~/workspace/golang/Dockerfile

docker image build -t enthusiastic/golang-tools:0.2 .

docker image push enthusiastic/golang-tools:0.2

```dockerfile
#Définition de l'image de base
FROM rastasheep/ubuntu-sshd:18.04

#On reprend les commandes en faisant attention à ce qu'elles puissent
#s'exécuter automatiquement
RUN apt update -y && \
    export DEBIAN_FRONTEND=noninteractive && \
    apt install apt-utils -y && \
    apt install software-properties-common -y && \
    add-apt-repository ppa:longsleep/golang-backports && \
    apt install golang-go -y && \
    apt install git -y && \
    go get github.com/nsf/gocode && \
    apt install iputils-ping -y && \
    apt install iproute2 -y && \
    apt install net-tools -y && \
    apt install vim -y

#Définir la commande à exécuter, elle ne doit pas forker
CMD ["/usr/sbin/sshd", "-D", "-p", "221"]

EXPOSE 221
```


## k3d

k3d create --publish 8080:80 --publish 22:22 --publish 220:220 --publish 221:221 --publish 222:222 --publish 223:223 --publish 224:224 --publish 225:225  --publish 226:226  --publish 227:227 --publish 228:228 --publish 229:229 --publish 230:230 --publish 231:232 --api-port 6550 --workers 2 --name ingress-istio --server-arg --no-deploy --server-arg traefik --auto-restart

sed -i '' 's/default/k3d-istio/g' $(k3d get-kubeconfig --name='ingress-istio')

sed -i '' 's/127.0.0.1/kubernetes.docker.internal/' $(k3d get-kubeconfig --name='ingress-istio')

export KUBECONFIG="$(k3d get-kubeconfig --name='ingress-istio')"

## Install Istio
cd ~/workspace
curl -L https://istio.io/downloadIstio | sh -
cd istio-1.4.2
export PATH=$PWD/bin:$PATH
istioctl manifest apply --set profile=demo

kubectl edit svc istio-ingressgateway -n istio-system
ports:
  - name: ansibletools
    nodePort: 31000
    port: 220
    protocol: TCP
    targetPort: 220
  - name: golangtools
    nodePort: 31001
    port: 221
    protocol: TCP
    targetPort: 221

kubectl get svc -n istio-system

## DNS

kubectl edit configmaps coredns -n kube-system


## To delete k3d cluster
k3d stop -n ingress-istio
k3d delete -n ingress-istio

## Kubernetes requirements

### Namespace tools

kubectl create namespace tools

kubectl label namespace tools istio-injection=enabled

### Change context
kubectl config set-context $(kubectl config current-context) --namespace=tools

### Deploy Services

istio/Gateways-1.0.yaml

kubectl apply -f ~/workspace/istio/Gateways-0.1.yml

```yml
---

apiVersion: networking.istio.io/v1alpha3
kind: Gateway
metadata:
  name: gw-ansibletools
  namespace: tools
spec:
  selector:
    app: istio-ingressgateway # use istio default controller
  servers:
  - port:
        number: 220
        name: ansibletools-220
        protocol: TCP
    hosts:
      - "*"

---

apiVersion: networking.istio.io/v1alpha3
kind: Gateway
metadata:
  name: gw-golangtools
  namespace: tools
spec:
  selector:
    app: istio-ingressgateway # use istio default controller
  servers:
  - port:
        number: 221
        name: golangtools-221
        protocol: TCP
    hosts:
      - "*"
```

istio/VirtualServices-1.0.yaml

kubectl apply -f ~/workspace/istio/VirtualServices-0.1.yml

```yml
---

apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: vs-ansibletools
  namespace: tools
spec:
  hosts:
  - "*"
  gateways:
  - gw-ansibletools # can omit the namespace if gateway is in same namespace as virtual service.
  tcp:
  - match:
    - port: 220
    route:
    - destination:
        host: ansibletools
        port:
          number: 220

---
  apiVersion: networking.istio.io/v1alpha3
  kind: VirtualService
  metadata:
    name: vs-golangtools
    namespace: tools
  spec:
    hosts:
    - "*"
    gateways:
    - gw-golangtools # can omit the namespace if gateway is in same namespace as virtual service.
    tcp:
    - match:
      - port: 221
      route:
      - destination:
          host: golangtools
          port:
            number: 221


```

ansible/golang-ubuntu-tools-1.0.yaml

kubectl apply -f ~/workspace/golang/golang-ubuntu-tools-0.1.yml
```yml

apiVersion: v1
kind: Service
metadata:
  name: golangtools
  labels:
    app: golangtools
    service: golangtools
  namespace: tools
spec:
  ports:
    - port: 221
  selector:
    app: golangtools
  clusterIP: None


---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: dpl-golang-tools
  labels:
    app: golangtools
  namespace: tools
spec:
  replicas: 1
  selector:
    matchLabels:
      app: golangtools
  template:
    metadata:
      labels:
        app: golangtools
    spec:
      containers:
        - name: golangtools
          image: enthusiastic/golang-tools:0.2
          ports:
            - containerPort: 221
```

ansible/ansible-ubuntu-tools-1.0.yaml

kubectl apply -f ~/workspace/ansible/ansible-ubuntu-tools-0.1.yml

```yml


apiVersion: v1
kind: Service
metadata:
  name: ansibletools
  labels:
    app: ansibletools
    service: ansibletools
  namespace: tools
spec:
  ports:
    - port: 220
  selector:
    app: ansibletools
  clusterIP: None


---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: dpl-ansible-tools
  labels:
    app: ansibletools
  namespace: tools
spec:
  replicas: 1
  selector:
    matchLabels:
      app: ansibletools
  template:
    metadata:
      labels:
        app: ansibletools
    spec:
      containers:
        - name: ansibletools
          image: enthusiastic/ansible-tools:0.3
          ports:
            - containerPort: 220

```



To delete:

kubectl delete -f ~/workspace/ansible/ansible-ubuntu-tools-0.1.yml

# K3D Traefik

k3d create --publish 9090:80 --api-port 6660 --workers 2 --name ingress-traefik --auto-restart