#!/bin/bash

sudo apt-get update && sudo apt-get install -y curl virtualbox jq
curl -LO https://storage.googleapis.com/minikube/releases/v1.5.2/minikube-linux-amd64 \
  && sudo install minikube-linux-amd64 /usr/local/bin/minikube
minikube start --memory 8192 --vm-driver=virtualbox --kubernetes-version v1.16.3
