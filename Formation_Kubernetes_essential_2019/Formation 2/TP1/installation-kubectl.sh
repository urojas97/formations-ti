#!/bin/bash

sudo apt-get update && sudo apt-get -y install curl
curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.16.3/bin/linux/amd64/kubectl
chmod +x kubectl
sudo mv kubectl /usr/local/bin

# On pensera à ajouter l'auto-completion dans son bashrc
echo "source <(kubectl completion bash)" >> ~/.bashrc
