# Formations NGINX essential

```dot
digraph notebook {
    Process -> Model
    Model -> "Event based"
    Model -> "Shared Memory"
    Process -> NGINX -> Master;
    Master -> Worker1;
    Master -> Worker2;
    Master -> Worker3;
}
```
## Intro

w3m est un navigateur web libre et open source en mode texte. 

Reverse Proxy
Web Server
Application Gateway (FastCGI, uWSGI)


```bash
ngnix -s reload
```
Replace worker by new worker with the new config

Shutdown
```bash
ngnix -s quit
ngnix -s stop
ngnix -t
```

Display running config
```bash
ngnix -T
```

Display version
```bash
nginx -v
```


## Files
Main config

    /etc/nginx/nginx.conf

Additional config (include directive)

    /etc/nginx/conf.d/*.conf

## Configuration context

### Directives
Http context
```nginx
server{
    listen{
        socket unix?;
        ip is prioriorated;
        default_server (when use server_name);
        443 ssl;
    }
    ssl_certificate;
    ssl_certificate_key;
    ssl_protocols;
    ssl_ciphers;
    ssl_prefer_server_ciphers;
    add_header{
        (HSTS){
            Strict-Transport-Security max-age=63072000;
            X-Content-Type-Options nosniff;
            X-Frame-Options DENY;
        }
    }
    ssl_stapling;
    ssl_dhparam;
    ssl_ecdh_curve;
    ssl_session_cache;
    ssl_session_timeout;
    ssl_session_tickets;
    root;
    server_name{
        wildcard fqdn;
        fqdn;
        regex{
            ~^(www|host1).*\.example\.com$;
        }
    }
    return 444; (break code)
        # 301 
    location{ #(gestion des paths et directives root)
        modifier;
            # = ~* ^~@
        proxy_pass <destination>;
            # Attention au path, il est remplace si on specifie un path apres la destination `/otherapp`
        index;
        limit_conn;
        limit_reg;
        limit_rate_after;
        limit_rate;
        alias;
        return ;
            # code url
            # url
        rewrite{
            regex replacement [tag];
            rewrite flags;
                # break
                # redirect
                # permanent
        }
        try_files;
        proxy_cache;
        proxy_cache_key;
        proxy_cache_valid;
    }
    error_log;  
        # debug info notice warn error crit alert emerg
    access_log;
        # default combined (format de log)
    limit_conn_zone;
    limit_reg_zone;
    match http {
        send "GET / HTTP/1.0\r\n Host: localhost:8080\r\n\r\n";
        expect ~* "200 OK";
        # match 
            # status
            # header
            # body
    }
    health_check interval=10 passes=3 fails=1 match=http;
}
log_format test_log;
    # utiliser pour customiser ses logs dans les context
    # custom forma log to replace combined
    # error_log
    # access_log
    # Request: $request\n Status: $status\n
    # Request_URI: $request_uri\n Host: $host\n
    # Client_IP: $remote_addr\n
    # Proxy_IP(s): $proxy_add_x_forwarded_for\n
    # Proxy_Hostname: $proxy_host\n
    # Real_IP: $http_x_real_ip"';
map;
    # cle/valeur (location/pathdata for example)

proxy_cache_path;
    # used for cache content
```  

    

**AND**

stream context
```nginx
upstream {
    # liste of server to loadbalance
        # poids, max failed, failed timeout
        # weight, max_fails, 
    server;
    least_time; #(only nginx+)
    hash;
    ip_hash;
    max_conns;
    queue;
    zone `<name>` `<memory>`;
        # zone memoire pour partager les informations de connection count
    state;
}
server {
    status_zone mycache;
}
```
    



## Rotating Logs
cron mieux vaut utiliser logrotate

## Security

```dot
digraph notebook {
    Module -> http_ssl_module;
    http_ssl_module -> Requirement -> openssl
    http_ssl_module -> Termination -> ssl -> Client
    Termination -> proxy_ssl -> "upstream\n(backend)"
}
```

HSTS -> force browser to communicate in https

Creation d'une pki
```bash
sudo openssl req -x509 -nodes -days 365 \
-newkey rsa:2048 -keyout nginx.key -out nginx.crt
```

ssllabs.com -> pour verifier la qualite du certificat ssl

    curl -k for ssl 

## Variables

```dot
digraph notebook {
    Variable -> "Core Module Variable"-> URL -> "$host";
    URL -> "$request_uri"
    "Core Module Variable"-> troubleshouting -> "$upstream_connect_time";
    Variable -> Custom -> set
    Variable -> Scope
    Variable -> map

}
```

`~^[23]` -> Start with 2 or 3

## Blocks

**Serving Content**
Blocks
- http
- server - virtual server
- location - URI

**Routing connections**

**Upstream**

```dot
digraph notebook {

    Module -> ngx_stream_core_module
    ngx_stream_core_module -> "Main Context"
    "Main Context" -> Upstream
    Upstream -> "DNS Loadbalancing\options"
     Module -> ngx_core_module -> "context\nserver/location"-> proxypass -> Upstream
}
```

## session affinity (only nginx+)

sticky cookie
sticky learn
sticky route

## api
demo.ngnix.com

http://nginx.org/en/docs/http/ngx_http_api_module.html

https://demo.nginx.com/swagger-ui/


http://docs.swagger.io/spec.html

## Monitoring
api
/dashboard
status_zone
zone
health_check

## Caching
hash
filesystem
memoire
proxy_cache_path
    http context
proxy_cache
proxy_cache_key
proxy_cache_valid


**Automation**
api write=on


# Formation Advanced load Balancing

```json
upstream myServers {
    server localhost:8080;
    server localhost:8081;
    server localhost:8082;
}
server {
    listen 80;
    root /usr/share/nginx/html;
    location / {
    proxy_pass http://myServers;
}
```

weighted-round-robin
ip_hash & hash
least_conn
least_time

## Lab 1,2,3,5,6
```bash
cd /etc/nginx/conf.d/
sudo mv default.conf default.conf.bak
```

### Server1
main.conf
```nginx
proxy_cache_path /data/nginx/cache levels=1:2 keys_zone=mycache:10m inactive=600s max_size=2G;

log_format sticky "Request: $request \nStatus: $status \nClient: $remote_addr\nUpstream IP: $upstream_addr \nURI: $route_uri \nCookie: $route_cookie";

map $cookie_jsessionid $route_cookie {
    ~.+\.(?P<route>\w+)$ $route;
}
map $request_uri $route_uri {
    ~jsessionid=.+\.(?P<route>\w+)$ $route;
}

upstream myServers {
    zone http_backend 64k;
    server ec2-13-56-20-152.us-west-1.compute.amazonaws.com;
    server ec2-13-56-211-188.us-west-1.compute.amazonaws.com:8080 route=backend1 backup;
    server ec2-13-56-19-160.us-west-1.compute.amazonaws.com:8080 route=backend2 backup;
    server ec2-54-219-164-27.us-west-1.compute.amazonaws.com:8080 route=backend3 backup;
    sticky route $route_cookie $route_uri;
}

server {
    listen 80;
    root /usr/share/nginx/html;
    proxy_cache mycache;
    proxy_cache_valid 200 5m;
    proxy_cache_key $scheme$host$request_uri;
    error_log /var/log/nginx/myServers.error.log info;
    access_log /var/log/nginx/myServers.access.log sticky;
    location / {
        proxy_pass http://myServers;
    }
    location /api {
        api write=on;
    }
    location /dashboard {
        try_files $uri $uri.html /dashboard.html;
    }
}
```
```bash
sudo nginx -t | sudo nginx -s reload
curl http://localhost/
```
lab5 nginx-sync
```bash
#Host1:
sudo ssh-keygen -t rsa -b 4096
sudo cat /root/.ssh/id_rsa.pub
#Host2
sudo vim /root/.ssh/<authorized_keys>
sudo vim /etc/ssh/sshd_config
    #PasswordAuthentication no
    #PermitRootLogin without-password
sudo service ssh restart
sudo service sshd restart
#Host1
sudo su
ssh -i /root/.ssh/id_rsa root@<Host-IP>
sudo vim /etc/nginx-sync.conf
    #NODES="ec2-13-56-20-152.us-west-1.compute.amazonaws.com"
    #CONFPATHS="/etc/nginx/nginx.conf /etc/nginx/conf.d"
    #EXCLUDE="web-server.conf.bak"
sudo /usr/bin/nginx-sync.sh
nginx-sync.sh -h #displays usage info
nginx-sync.sh -c <BACKUP> #compares host1 and host2
#Host2
sudo cat /etc/nginx/conf.d/main.conf
```

keepalived
```bash
apt-get install nginx-ha-keepalived
nginx-ha-setup
```
```json
    vrrp_script chk_nginx_service {
        script "/usr/libexec/keepalived/nginx-ha-check"
        interval 3
        weight 50
    }
```
```bash
#Host 1 & 2
cd /usr/bin
sudo ./nginx-ha-setup
#Host1
cat /var/run/nginx-ha-keepalived.state
    #STATE=MASTER
#Host2 
cat /var/run/nginx-ha-keepalived.state
    #STATE=BACKUP
#Host1
sudo service keepalived stop
cat /var/run/nginx-ha-keepalived.state
#host2
cat /var/run/nginx-ha-keepalived.state
```

Advanced VRRP
```json
    vrrp_instance VI_1 {
        interface eth0
        priority 101
        virtual_router_id 51
        advert_int 1
        accept
        garp_master_refresh 5
        garp_master_refresh_repeat 1
        unicast_src_ip 192.168.100.100
        unicast_peer {
            192.168.100.101
        }
        virtual_ipaddress {
            192.168.100.150
        }
    }
    vrrp_script chk_manual_failover {
        script "/usr/libexec/keepalived
        /nginx-ha-manual-failover"
        interval 10
        weight 50
    vrrp_script chk_nginx_service {
        script "/usr/libexec/keepalived
        /nginx-ha-check"
        interval 3
        weight 50
    }
```

Multiple virtual ip
```json
    virtual_ipaddress {
        192.168.100.150
        192.168.100.200
    }
```

VRRP A/A
```json
    vrrp_instance VI_1 {
        interface eth0
        state BACKUP
        priority 101
        virtual_router_id 51
        ...
        unicast_src_ip 192.168.10.10
        unicast_peer {
            192.168.10.11
        }
        virtual_ipaddress {
            192.168.10.100
        }
        ...
    }
    vrrp_instance VI_2 {
        interface eth0
        state BACKUP
        priority 100
        virtual_router_id 61
        ...
        unicast_src_ip 192.168.10.10
        unicast_peer {
            192.168.10.11
        }
        virtual_ipaddress {
            192.168.10.101
        }
        ...
    }
```

NGINX A/A
```json
    server {
        listen 192.168.10.100:80;
        location / {
            root /application1;
        }
    }
    server {
        listen 192.168.10.101:80;
        location / {
            root /application2;
        }
    }

    server {
        listen *:80;
        location /app1 {
            root /application1;
        }
        location /app2 {
            root /application2;
        }
    }
```
## Directives

### if
```nginx
    if ($request_method = POST ) {
        return 405;
    }
    ---------------------------------------------------
    if ($args ~ post=140){
        rewrite ^ http://example.com/ permanent;
    }
```

### try_files
```nginx
    location / {
        try_files $uri $uri/ @proxy;
    }
    location @proxy {
        proxy_pass http://backend/index.php;
    }
```

### error_page
```nginx
    error_page 404 /404.html;
    location = /404.html {
        root /usr/share/nginx/html;
    }
    error_page 500 502 503 504 /50x.html;
    location /50x.html {
        root /usr/share/nginx/html;
    }
```

### stub_status
```nginx
    location /status {
        stub_status on;
    }
    location /api {
        api;
    }
```
### sticky ( Session affinity; state data on backends servers)
- cookie
- route
- learn ( Sessionid; mapping table)
```nginx
    upstream myServers {
        server backend1;
        server backend2;
        server backend3;
        sticky cookie my_srv expires=1h domain=example.com path=/cart;
    }

    upstream myServers {
        zone backend 64k;
        server backend1 route=backend1;
        server backend2 route=backend2;
        server backend3 route=backend3;
        sticky route $route_cookie $route_uri;
    }

    map $cookie_JSESSIONID $route_cookie {
        ~.+\.(?P<route>\w+)$ $route;
    }
    map $request_uri $route_uri {
        ~JSESSIONID=.+\.(?P<route>\w+)$ $route;
    }

    upstream myServers {
        server backend1;
        server backend2;
        server backend3;
        sticky learn create=$upstream_cookie_sessionid lookup=$cookie_sessionid zone=client_
    }

    server {
        location / {
            proxy_pass http://myServers;
        }
    }
```
### http vs Stream
```nginx
    stream {
        upstream postgres-upstream {
            server postgres.host1:5432;
            server postgres.host2:5432;
        }
        server {
            listen 5432;
            proxy_pass postgres-upstream;
        }
    }


    stream {
        server {
            listen 443;
            ssl_preread on;
            proxy_pass $ssl_preread_server_name;
        }
    }

    log_format tcp_log '$remote_addr [$time_local] ' '$protocol $status $bytes_sent $bytes_receveid.....
```
### Health Check
```nginx
    server {
        listen 8080;
        ...
        health_check interval=10 passes=3 fails=1 match=http;
    }

    match http {
        send "GET / HTTP/1.0\r\n Host: localhost:8080\r\n\r\n";
        expect ~* "200 OK";
    }
```

### UDP
```nginx
    stream {
        match udp_test {
            send \x00\x2a\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\
            x03\x73\x74\x6c\x04\x75\x6d\x73\x6c\x03\x65\x64\
            x75\x00\x00\x01\x00\x01;
            expect ~* "health.is.good";
    }
    upstream dns_upstream {
        server dns1:53;
        server dns2:53;
    }
    server {
        listen 53 udp;
        proxy_pass dns_upstream;
        health_check match=udp_test udp;
    }
```
### Ip Transparency
```nginx
    upstream db {
        server db1:3306;
        server db2:3306 backup;
        server db3:3306 down;
    }
    server {
        listen 3306;
        proxy_pass db;
        proxy_connect_timeout 1s;
        proxy_bind $remote_addr transparent;
        proxy_pass http://mysql_db_upstream;
    }
```
### Proxy Protocol
```nginx
    stream {
        server {
            listen 12345;
            proxy_pass example.com:12345;
            proxy_protocol on;
        }
    }

    log_format combined '$proxy_protocol_addr - $remote_user [$time_local] ''"$request" $status $body_bytes_sent ''"$http_referer" "$http_user_agent"';
    server {
        listen 80 proxy_protocol;
        listen 443 ssl proxy_protocol;
        set_real_ip_from 192.168.1.0/24;
        real_ip_header proxy_protocol;
        location /app {
            proxy_pass http://backend1;
            proxy_set_header X-Real-IP $proxy_protocol_addr;
            proxy_set_header X-Forwarded-For $proxy_protocol_addr;
        }
    }
```
### DSR (offload)
```nginx
    server {
        listen 53 udp;
        proxy_bind $remote_addr:$remote_port transparent;
        proxy_responses 0;
        # proxy_timeout 1s;
    }
```

### Tip and tricks NGINX+
```nginx
    upstream mongo_shard_tier {
        hash $remote_addr consistent;
        server mongo.replica1:27017;
        server mongo.replica2:27017;
    }

    upstream ldap_upstream {
        least_conn;
        server ldap1:389 slow_start=60s;
        server ldap2:389 slow_start=60s;
    }

    upstream appservers {
        zone appservers 64k;
        server appserv1.example.com:12345;
        server backup1.example.com:12345 backup;
        server backup2.example.com:12345 backup;
    }
    server {
        proxy_pass appservers;
        health_check;
    }

    server {
        listen 443 ssl;
        include /etc/ssl/ssl-params.conf;
        location /api {
            limit_except GET {
                auth_jwt "+ API" token=$arg_apijwt;
                auth_jwt_key_file <secret>.jwk;
            }
            api write=on;
            allow 127.0.0.1;
            deny all;
        }
    }
```
```bash
    curl -k -X POST -d '{\"server": "appserv2.example.com:12345",\"slow_start": "60s"}' -s 'https://127.0.0.1/api/3/\stream/upstreams/appservers/servers?apijwt=<token>'
```
### Caching
```nginx
    proxy_cache_path /data/nginx/cache levels=1:2 keys_zone=my_cache:20m inactive=5m;
        server {
            proxy_cache_key $scheme$host$request_uri;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            ...
            location /application1 {
                proxy_cache my_cache;
                proxy_cache_valid any 10m;
                proxy_pass https://backend.server;
            }
        }
```
### Sharded Chaching
```nginx
    upstream cache-servers {
        hash $scheme$proxy_host$request_uri consistent;
        server cache-server1;
        server cache-server2;
        server cache-server3;
    }
```

### High availlability Cache

#### Primary
```nginx
    proxy_cache_path /tmp/mycache keys_zone=mycache:10m;
    server {
        status_zone mycache; # for NGINX Plus status dashboard
        listen 80;
        proxy_cache mycache;
        proxy_cache_valid 200 15s;
        location / {
            proxy_pass http://secondary;
        }
    }
    upstream secondary {
        zone secondary 128k; # for NGINX Plus status dashboard
        server 192.168.56.11; # secondary
        server 192.168.56.12 backup; # origin
    }
```
#### Second
```nginx
    proxy_cache_path /tmp/mycache keys_zone=mycache:10m;
    server {
        status_zone mycache; # for NGINX Plus status dashboard
        listen 80;
        proxy_cache mycache;
        proxy_cache_valid 200 15s;
        location / {
            proxy_pass http://origin;
        }
    }
    upstream origin {
        zone origin 128k; # for NGINX Plus status dashboard
        server 192.168.56.12; # origin
    }
```

###Hot Cache
    keepalived
    hash consistent
    expires

```bash
nginx-sync.sh
    nginx-sync.conf
        NODES="node2.example.com node3.example.com node4.example.com"
        CONFPATHS="/etc/nginx/nginx.conf /etc/nginx/conf.d"
        EXCLUDE="web-server.conf.bak"
``` 

resolver 10.0.0.53 valid=20s;
```nginx
server {
    listen 9000;
    zone_sync;
    zone_sync_server cluster.example.com:9000 resolve;
}
```

```nginx
limit_req_zone $remote_addr zone=per_ip:1M rate=100r/s sync;
# Cluster-aware rate limit
limit_req_status 429;
# Response code sent to client
keyval_zone zone=sinbin:1M timeout=600 sync;
# Cluster-aware "sin bin" with 10-minute TTL
keyval $remote_addr $in_sinbin zone=sinbin;
# Populate $in_sinbin with matched client IP addresses
```

## Lab 4

/etc/nginx/nginx.conf
```nginx
    stream {
        include /etc/nginx/tcp/*.conf;
    }
```
sudo mkdir -p /etc/nginx/tcp
sudo vim /etc/nginx/tcp/tcp_lb.conf


tcp_lb.conf
```nginx
    upstream tcp_backend {
        zone tcp_backend 64k;
        server ec2-13-56-211-188.us-west-1.compute.amazonaws.com:8080;
        server ec2-13-56-19-160.us-west-1.compute.amazonaws.com:8080;
        server ec2-54-219-164-27.us-west-1.compute.amazonaws.com:8080;
    }
    server {
        listen 8080;
        proxy_pass tcp_backend;
        health_check interval=10 passes=3 fails=1 match=http;
    }
    match http {
        send "GET / HTTP/1.0\r\n Host: localhost:8080\r\n\r\n";
        expect ~* "200 OK";
    }
```

https://www.nginx.com/resources/wiki/start/topics/tutorials/config_pitfalls/



# Epsilamic Formation (NobleProg)

3 Days
```bash
openssh s_client -connect ville.montreal.qc.ca:443
```


```dot
digraph notebook {
    Clients -> Accelerator
    Accelerator -> Static_file
    Accelerator -> App1
    Accelerator -> App2
    Accelerator -> Auth_IDP
    subgraph cluster_0 {
        subgraph cluster_0 {
            label="NGINX";
            Accelerator
        }
        label="Simple Page Application";
        Accelerator ; Static_file ; App1 ; App2 ; Auth_IDP
    }

}
```

```dot
digraph notebook {
    subgraph cluster_1 {
        Request -> Master_Process
        Master_Process -> WK1
        Master_Process -> WK2
        Master_Process -> WK3
        WK1 -> CPU1
        WK2 -> CPU2
        WK3 -> CPU3
        subgraph cluster_0 {
            label=NGINX;
            Master_Process ; WK1 ; WK2 ; WK3
            subgraph cluster_0 {
                label=worker
                WK1 ; WK2 ; WK3
            }
            subgraph cluster_1 {
                label=optional_affinity
                CPU1 ; CPU2 ; CPU3
            }
    }
   }
   }
```

```dot
digraph notebook {
    "nginx.conf" -> includes
    includes -> "sites-enabled" -> "sites-available"
    includes -> "conf.d" -> "files.conf"
    subgraph cluster_0 {
        label=symlink
        "sites-enabled" ; "sites-available"
    }
}
```

`nginx -t` avant de faire un `service nginx reload`

```nginx
server {
    listen 80 default_server;
    server_name ww.myapp.io;
    root /var/data;
    error_page 404 =400  @maintenance;

    location /files {
        alias /var/myapp_data;
    }
    location ~ ^/(.*\.pdf)$ {
        root /var/myapp_data;
    }
    location / {
        root /var/myapp_code;
        error_page 404 /error;
    }
    location /error {
        return 200 "error";
    }
    location /example {
        types {} default_type application/json;
        return 200 "{ \"error\": \"Service unaivalaible\"}"
    }
    location /api {
        ....
        error_page 502 =200 /maintenance
    }
    location /maintenance {
            internal;
            index maintenance.html
            alias /var/www/errorpages;

    }
    location @maintenance {
        return 200 "Maintenance mode";
    }


    ...tof

    return 301

    location /request_file {
        rewrite ^/request_file?n=([0-9])$ /file$1.txt last;
    }

    location /request_file {
        return 301 /file$arg_n.txt;
    }



    location /request_file {
        if($arg_password ="letmein) {
            rewrite .* /file$arg_n.txt last;
        }
        return 403;
    }

    location = /empty.git {
        empty_gif;
    }

    location /api {
        add_header X-Custom value;
        default_type application/json;
        return 503 "{\"error\":}"
    }

    location ~ ^/api/.*\.php$ {

    }
}
```
```http
?n=1&password=letmein
```
    mime.types
        by default application/octet-stream


| Rules\Request      | /files/foo.txt |  /documet.pdf | /api  |
|---                 |:---:           |:---:          |:---:  |
| location /files    | V(LP)          | X             | X     |
| location ~ \.pdf$  | X              | V             | X     |
| location ~* \.pdf$ | X              | V             | X     |
| location /         | V              | V             | V(LP) |
| location ^~ /      | V              | V             | V(LP) | 
| location = /       | X              | V             | X     | 

```dot
digraph notebook {
    Client -> Server ->
    "Http/1.1 301\nlocation /api/.." -> Client ->
    "GET Http://newurl/api/..." -> Server
    subgraph cluster_0 {
        label=redirection
        Client 
        Server
        "Http/1.1 301\nlocation /api/.."
        Client
        "GET Http://newurl/api/..."
    }
    Client1 -> "Get /foo" -> Server1 -> "* +/-headers" -> Server2 -> Server1 -> Client1
    subgraph cluster_1 {
        label=proxypass
        Client1
        "Get /foo" 
        Server1 "* +/-headers"
        Server2
    }
}
```

    Upstream Redirect NGINX
        proxy_pass  ->
        Upstream -> X-Accel-Redirect: /protected
        location /protected {internal}


    Other protocols:
    websocket
        CGI
        php-fpm is a FastCGI implimetation or PHP
        file: fastcgi_params

Certbot


## LABs

```bash
# VM Setup

# You are assigned a number (1-9) that determines your VM "group". 
# Each logical group consists of 3 VMs. 

# To access a VM, first SSH into the jumpbox: admin@66.70.255.226
# From the jumpbox SSH into your VMs by "name" (from /etc/hosts):
ssh vm7A
ssh vm7B
ssh vm7C

# In your home directory, untar setup.tgz

# Use ./expose.sh and ./unexpose.sh to bind your public-facing NIC to a publicly-routable IP

# Run ./nodeinstall.sh to install Node.js

# To deploy backend for the sample application, follow these steps:

# Untar server.tgz
mkdir server
cd server
tar xf ../server.tgz .

# Pull dependencies
npm install

# Start application
nohup npm start &

# Stop application
killall node


# Part I: BASICS
# Create your own conf file 
# Put the following content into /etc/nginx/sites-available/lab

sudo tee /etc/nginx/sites-available/lab <<-EOF
```
```nginx
server {
    listen 80;
    return 500;
}
```
```bash
EOF
```
```bash
# Unlink the default configuration
rm /etc/nginx/sites-enabled/default

# Link the new configuration
ln -sv /etc/nginx/sites-available/lab /etc/nginx/sites-enabled

# Make sure that /etc/nginx/nginx.conf uses sites-enabled. Add the following line
cat /etc/nginx/nginx.conf | grep "include /etc/nginx/sites-enabled/*;"

# remove line:
include /etc/nginx/conf.d/*;

# To test configuration and reload, execute:
nginx -t && sudo service nginx reload

# Observe how your server return a 500 status page. Change status code and see how it reacts.
curl 51.79.106.247

return 400; # Try this and reload

sudo tee /etc/nginx/sites-available/lab <<-EOF
```
```nginx
server {
    listen 80;
    return 400;
}
```
```bash
EOF
```
```bash
nginx -t && sudo service nginx reload
curl 51.79.106.247

# Replace the “return” line with the following:
        default_type text/html;
        return 200 "<h1>Hello, world!</h1>";

sudo tee /etc/nginx/sites-available/lab <<-EOF
```
```nginx
server {
    listen 80;
    default_type text/html;
    return 200 "<h1>Hello, world!</h1>";
}
```
```bash
EOF
```
```bash
# Reload and see the HTML being rendered
sudo nginx -t && sudo service nginx reload
curl 51.79.106.247

# Create a directory to keep static files to serve
sudo mkdir /var/static-files
cd /var/static-files
sudo wget https://picsum.photos/300 -O picture.jpg
echo "<h1>Test page</h1>" > page.html

sudo tee /var/static-files/page.html <<-EOF
```
```html
<h1>Test page</h1>
```
```bash
EOF
```
```bash
sudo mkdir level1
cd level1
sudo wget https://picsum.photos/300 -O picture.jpg

# In config file, replace the return and type lines with:
        root /var/static-files;

sudo tee /etc/nginx/sites-available/lab <<-EOF
```
```nginx
server {
    listen 80;
    default_type text/html;
    root /var/static-files;
}
```
```bash
EOF
```
```bash
# After reloading, try to request: 
http://<public ip> # Returns a 403
http://<public ip>/page.html
http://<public ip>/picture.jpg
http://<public ip>/level1/picture.jpg

sudo nginx -t && sudo service nginx reload

curl http://51.79.106.247
curl http://51.79.106.247/page.html
curl http://51.79.106.247/picture.jpg
curl http://51.79.106.247/level1/picture.jpg

# Now, enclose the root in a location. Should still work the same.
```
```nginx
location / {
        root /var/static-files;

}
```
```bash

sudo tee /etc/nginx/sites-available/lab <<-EOF
```
```nginx
server {
    listen 80;
    default_type text/html;
    location / {
        root /var/static-files;
    }
}
```
```bash
EOF
```
```bash
sudo nginx -t && sudo service nginx reload

# Change location to level1:
```
```nginx
location /level1 {
        root /var/static-files;
}
```
```bash

sudo tee /etc/nginx/sites-available/lab <<-EOF
```
```nginx
server {
    listen 80;
    default_type text/html;
    location /level1 {
        root /var/static-files;
    }
}
```
```bash
EOF
```
```bash
sudo nginx -t && sudo service nginx reload

curl http://51.79.106.247
curl http://51.79.106.247/page.html
curl http://51.79.106.247/picture.jpg
curl http://51.79.106.247/level1/picture.jpg

# Now, only http://<public ip>/level1/picture.jpg works
# Change "root" to "alias"
        alias /var/static-files;

sudo tee /etc/nginx/sites-available/lab <<-EOF
```
```nginx
server {
    listen 80;
    default_type text/html;
    location /level1 {
        alias /var/static-files;
    }
}
```
```bash
EOF
```
```bash
sudo nginx -t && sudo service nginx reload

curl http://51.79.106.247
curl http://51.79.106.247/page.html
curl http://51.79.106.247/picture.jpg
curl http://51.79.106.247/level1/picture.jpg

# Try the new URIs of all the files. Note the difference in how "alias" and "root" handle location prefixes

http://<public ip>/level1/page.html
http://<public ip>/level1/picture.jpg
http://<public ip>/level1/level1/picture.jpg

curl http://51.79.106.247/level1/page.html
curl http://51.79.106.247/level1/picture.jpg
curl http://51.79.106.247/level1/level1/picture.jpg

# Go back to using "root" and "location /"

# Add an "error_page" directive and a named location:
```
```nginx
        location / {
                root /var/static-files;
                error_page 404 @error;
        }

        location @error {
                default_type text/html;
                return 200 "Custom error message";
        }
```
```bash
sudo tee /etc/nginx/sites-available/lab <<-EOF
```
```nginx
server {
    listen 80;
    default_type text/html;
    location / {
        root /var/static-files;
        error_page 404 @error;
    }
    location @error {
        default_type text/html;
        return 200 "Custom error message";
    }
}
```
```bash
EOF
```
```bash
sudo nginx -t && sudo service nginx reload

# Request a non-existent location and observe the message from @error returned.
# Note how response code has been changed by error_page
curl http://51.79.106.247/level2

# Now, change error_page line to :
error_page 404 =400 @error;

sudo tee /etc/nginx/sites-available/lab <<-EOF
```
```nginx
server {
    listen 80;
    default_type text/html;
    location / {
        root /var/static-files;
        error_page 404 =400 @error;
    }
    location @error {
        default_type text/html;
        return 200 "Custom error message";
    }
}
```
```bash
EOF
```
```bash
sudo nginx -t && sudo service nginx reload

# Observe how the error code from the @error location changed to 400
curl -v http://51.79.106.247/level2

# Create a custom error html page:
echo "<h2>A custom error page</h2>" > /var/static-files/error404.html

sudo tee /var/static-files/error404.html <<-EOF
```
```html
<h2>A custom error page</h2>
```
```bash
EOF
```
```bash

# Note how you can request http://<public ip>/error404.html (without reload)
# Change @error to /error404.html in the error_page line, observe the changed error page when requesting non-existent locations

curl http://51.79.106.247/error404.html

sudo tee /etc/nginx/sites-available/lab <<-EOF
```
```nginx
server {
    listen 80;
    default_type text/html;
    location / {
        root /var/static-files;
        error_page 404 =400 /error404.html;
    }
    location @error {
        default_type text/html;
        return 200 "Custom error message";
    }
}
```
```bash
EOF
```
```bash
sudo nginx -t && sudo service nginx reload

curl -v http://51.79.106.247/level2

# Let's disallow requesting /error404.html directly

# Create a location for the page:
```
```nginx
location /error404.html {
    root /var/static-files;
}
```
```bash

sudo tee /etc/nginx/sites-available/lab <<-EOF
```
```nginx
server {
    listen 80;
    default_type text/html;
    location / {
        root /var/static-files;
        error_page 404 =400 /error404.html;
    }
    location /error404.html {
        root /var/static-files;
    }
    location @error {
        default_type text/html;
        return 200 "Custom error message";
    }
}
```
```bash
EOF
```
```bash
sudo nginx -t && sudo service nginx reload

curl -v http://51.79.106.247/level2

# Nothing should have changed so far

# Now, mark the new location as internal:
```
```nginx
location /error404.html {
    internal;
    root /var/static-files;
}
```
```bash

sudo tee /etc/nginx/sites-available/lab <<-EOF
```
```nginx
server {
    listen 80;
    default_type text/html;
    location / {
        root /var/static-files;
        error_page 404 =400 /error404.html;
    }
    location /error404.html {
        internal;
        root /var/static-files;
    }
    location @error {
        default_type text/html;
        return 200 "Custom error message";
    }
}
```
```bash
EOF
```
```bash
sudo nginx -t && sudo service nginx reload

curl -v http://51.79.106.247/level2

# Observe that requests to /error404.html serve up Nginx's generic 404 page. This becomes more useful with pages for status codes other than 404.

curl -v http://51.79.106.247/level2

# Set up a regex location matching all .jpg files, and handle 404 errors by returning an empty gif:
```
```nginx
location ~ \.jpg$ {
        root /var/static-files;
        error_page 404 @emptyimage;
}

location @emptyimage {
        empty_gif;
}
```
```bash

sudo tee /etc/nginx/sites-available/lab <<-EOF
```
```nginx
server {
    listen 80;
    default_type text/html;
    location / {
        root /var/static-files;
        error_page 404 =400 /error404.html;
    }
    location /error404.html {
        internal;
        root /var/static-files;
    }
    location @error {
        default_type text/html;
        return 200 "Custom error message";
    }
    location ~ \.jpg$ {
        root /var/static-files;
        error_page 404 @emptyimage;
    }

    location @emptyimage {
        empty_gif;
    }
}
```
```bash
EOF
```
```bash
sudo nginx -t && sudo service nginx reload

# Try to request /somethingnotthere.jpg -> observe empty image instead of 404 page

curl -v http://51.79.106.247/somethingnotthere.jpg

# Try this more complicated example that simulates a password check:
```
```nginx
location ~ (?<password>[A-Za-z0-9]*)/(?<imagepath>[A-Za-z0-9-_]*\.jpg)$ {

        if ( $password != "letmein" ) {
                return 403;
        }

        root /var/static-files;
        error_page 404 @emptyimage;
        try_files /$imagepath =404;
}
```
```bash
sudo tee /etc/nginx/sites-available/lab <<-EOF
```
```nginx
server {
    listen 80;
    default_type text/html;
    location / {
        root /var/static-files;
        error_page 404 =400 /error404.html;
    }
    location /error404.html {
        internal;
        root /var/static-files;
    }
    location @error {
        default_type text/html;
        return 200 "Custom error message";
    }
    location ~ (?<password>[A-Za-z0-9]*)/(?<imagepath>[A-Za-z0-9-_]*\.jpg)$ {
        if ( \$password != "letmein" ) {
            return 403;
        }
        root /var/static-files;
        error_page 404 @emptyimage;
        try_files /\$imagepath =404;
    }

    location @emptyimage {
        empty_gif;
    }
}
```
```bash
EOF
```
```bash
sudo nginx -t && sudo service nginx reload

# http://<public ip>/letmein/picture.jpg will serve the photo. 
# Note the use of named capture groups in location regex - they become Nginx variables to use
# with the location block

http://51.79.106.247/letmein/picture.jpg
http://51.79.106.247/letmeina/picture.jpg
http://51.79.106.247/letmein/picturea.jpg

# Note how we use try_files - it tells Nginx to look up files by path, relative to "root" (/var/static-files)


# Part II: HTTP Proxy 

# Start over with a new config file. Add a location with proxy_pass to one of your other VMs:
```
```nginx
server {
        listen 80;
        location /api {
                # The IP below should be your different - use the one from another VM
                proxy_pass http://192.168.100.137:5000;
        }
}
```
```bash

sudo tee /etc/nginx/sites-available/lab <<-EOF
```
```nginx
server {
    listen 80;
    location /api {
        proxy_pass http://192.168.100.123:5000;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    }
}
```
```bash
EOF
```
```bash
sudo nginx -t && sudo service nginx reload

# SSH into the other VM and use nc -l 5000 to look at the requests that would come through.
# Notice that proxy headers with real ip are absent, and Host is not set

# Set up another proxy on the other VM to your third VM:
```
```nginx
server {
        listen 5000;
        location / {
                proxy_pass http://192.168.100.135:5000;
        }
}
```
```bash


sudo tee /etc/nginx/sites-available/lab <<-EOF
```
```nginx
log_format  lab  '\$remote_addr - \$remote_user [\$time_local] "\$request" '
                      '\$status \$body_bytes_sent "\$http_referer" '
                      '"\$http_user_agent" XFF "\$http_x_forwarded_for"';
server {
    listen 5000;
    location /api {
        proxy_pass http://192.168.100.124:5000;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        set_real_ip_from 192.168.100.122;
    }
    access_log  /var/log/nginx/access.log  lab;
}
```
```bash
EOF
```
```bash
sudo nginx -t && sudo service nginx reload

rm /etc/nginx/sites-enabled/default
ln -sv /etc/nginx/sites-available/lab /etc/nginx/sites-enabled
cat /etc/nginx/nginx.conf | grep "include /etc/nginx/sites-enabled/*;"
    # remove line:
        include /etc/nginx/conf.d/*;

sudo nc -l 5000

# On the third VM, run nc -l 5000 and look at the request headers as you request /api from first VM's public IP address

tar xvzf setup.tgz

./nodeinstall.sh

mkdir server
cd server
tar xf ../server.tgz .

npm install
nohup npm start &
killall node

sudo tee /etc/nginx/sites-available/lab <<-EOF
```
```nginx
log_format  lab  '\$remote_addr - \$remote_user [\$time_local] "\$request" '
                      '\$status \$body_bytes_sent "\$http_referer" '
                      '"\$http_user_agent" XFF "\$http_x_forwarded_for"';
server {
    listen 5000;
    default_type text/html;
    location /api {
        return 200 \$hostname;
    }
    access_log  /var/log/nginx/access.log  lab;
}
```
```bash
EOF
```
```bash
sudo nginx -t && sudo service nginx reload

rm /etc/nginx/sites-enabled/default
sudo ln -sv /etc/nginx/sites-available/lab /etc/nginx/sites-enabled
cat /etc/nginx/nginx.conf | grep "include /etc/nginx/sites-enabled/*;"
    # remove line:
        include /etc/nginx/conf.d/*;


sudo nginx -t && sudo service nginx reload

sudo nc -l 5000

# Pass a few headers from first VM with `proxy_set_header`
```

## HA with keepalived
Your VMs already have keepalived (compiled from source), which you can find at this location: /root/keepalived-2.0.16/bin/keepalived

You can also find a starter config file for keepalived at /root/keepalived.conf

### Initial setup
Pick two VMs that will be the in the HA cluster. If you installed the "shopping cart" application from the previous day on one VM, pick the other two VMs.
These notes assume that you picked vmA and vmB for the HA cluster.

Run sudo ./unexpose.sh on all VMs.
```bash
sudo ./unexpose.sh
```

Run sudo ifconfig ens160 up on the VMs in the HA cluster.

```bash
sudo ifconfig ens160 up
```
Take note of your public IP address (run ./getip.sh)
```bash
./getip.sh
```
`51.79.106.247`

On each VM in HA cluster, edit keepalived.conf

change IP in unicast_peer section to the private IP of the other VM.
change IP in virtual_ipaddress section to the public IP
On each VM, set up an Nginx location to send back the hostname of the VM:

```bash
sudo su -
```

#### Server A
```bash
sudo tee /root/keepalived.conf <<-EOF
```
```json
global_defs {
    vrrp_version 3
}


vrrp_track_file chck {
        file "/root/status"
        weight 50
        init_file 0
}


vrrp_instance VI_1 {
    interface                  ens192
    priority                   101
    advert_int                 1
    accept
    virtual_router_id          51

    unicast_peer {
        192.168.100.123
    }

    virtual_ipaddress {
        51.79.106.247 dev ens160
    }


        notify_master "/bin/bash -c '(route add 54.38.17.254 dev ens160 && echo Wait && route -n &&  sleep 5 && route -n && route add default gw 54.38.17.254) > /tmp/ok 2>&1'"


    track_file {
        chck
    }
}
```
```bash
EOF
```
#### Server B
```bash
sudo tee /root/keepalived.conf <<-EOF
```
```json
global_defs {
    vrrp_version 3
}


vrrp_track_file chck {
        file "/root/status"
        weight 50
        init_file 0
}


vrrp_instance VI_1 {
    interface                  ens192
    priority                   101
    advert_int                 1
    accept
    virtual_router_id          51

    unicast_peer {
        192.168.100.122
    }

    virtual_ipaddress {
        51.79.106.247 dev ens160
    }


        notify_master "/bin/bash -c '(route add 54.38.17.254 dev ens160 && echo Wait && route -n &&  sleep 5 && route -n && route add default gw 54.38.17.254) > /tmp/ok 2>&1'"


    track_file {
        chck
    }
}
```
```bash
EOF
```


On each VM, set up an Nginx location to send back the hostname of the VM:

#### Server A

```bash
sudo tee /etc/nginx/sites-available/lab <<-EOF
```
```nginx
server {
    location /hostname {
        default_type text/html;
        return 200 \$hostname;
    }
}
```
```bash
EOF
```
```bash
sudo nginx -t && sudo service nginx reload
```

#### Server B
```bash
sudo tee /etc/nginx/sites-available/lab <<-EOF
```
```nginx
server {
    location /hostname {
        default_type text/html;
        return 200 \$hostname;
    }
}
```
```bash
EOF
```
```bash
sudo nginx -t && sudo service nginx reload
```

On both VMs, run keepalived: sudo /root/keepalived-2.0.16/bin/keepalived -f /root/keepalived.conf

```bash
./keepalived-2.0.16/bin/keepalived -nlf keepalived.conf
```

Watch the logs and see which node transitioned to MASTER, and which transitioned to BACKUP

Run ifconfig on both VMs - make sure the master node has public IP set on ens160, and that the backup node does not have public IP set.

curl/browse to <public ip>/hostname - you should see the name of the the VM in the master state
```bash
curl -v http://51.79.106.247/hostname
```

Currently, the keepalived config is set to track a file as a "health check". After starting keepalived you should find a file, /root/status, with a content of "0". If the content of the file is changed to a negative number, keepalived will consider that a failure. This is useful for debugging keepalived before developing a real healh-check script.

Execute echo -100 > /root/status on the master VM. You should see the a transition to "failed" state in the output for that node, and a transition to "master" state in the output of the other node.
```bash
echo -100 > /root/status
```
Re-request <public ip>/hostname - you should see that the hostname changed.
```bash
curl -v http://51.79.106.247/hostname
```
Develop a health-check
Remove vrrp_track_file section in keepalived.conf and replace it with:
```nginx
vrrp_script nginx_hc {
  script       "/root/nginx_hc.sh"
  interval 2   # check every 2 seconds
}
```
Remove track_file section in keepalived.conf and replace it with:
```nginx
track_script {
    nginx_hc
}
```

#### Server A
```bash
sudo tee /root/keepalived.conf <<-EOF
```
```json
global_defs {
    vrrp_version 3
}


vrrp_script nginx_hc {
  script       "/root/nginx_hc.sh"
  interval 2   # check every 2 seconds
}


vrrp_instance VI_1 {
    interface                  ens192
    priority                   101
    advert_int                 1
    accept
    virtual_router_id          51

    unicast_peer {
        192.168.100.123
    }

    virtual_ipaddress {
        51.79.106.247 dev ens160
    }


        notify_master "/bin/bash -c '(route add 54.38.17.254 dev ens160 && echo Wait && route -n &&  sleep 5 && route -n && route add default gw 54.38.17.254) > /tmp/ok 2>&1'"


    track_script {
        nginx_hc
    }
}
```
```bash
EOF
```


####Server B
```bash
sudo tee /root/keepalived.conf <<-EOF
```
```json
global_defs {
    vrrp_version 3
}


vrrp_script nginx_hc {
  script       "/root/nginx_hc.sh"
  interval 2   # check every 2 seconds
}


vrrp_instance VI_1 {
    interface                  ens192
    priority                   101
    advert_int                 1
    accept
    virtual_router_id          51

    unicast_peer {
        192.168.100.122
    }

    virtual_ipaddress {
        51.79.106.247 dev ens160
    }


        notify_master "/bin/bash -c '(route add 54.38.17.254 dev ens160 && echo Wait && route -n &&  sleep 5 && route -n && route add default gw 54.38.17.254) > /tmp/ok 2>&1'"


    track_script {
        nginx_hc
    }
}
```
```bash
EOF
```


Put a one-line curl invocation to your Nginx, into a new script /root/nginx_hc.sh:
curl -f http://localhost/hostname
Make sure to make these changes on both HA VMs

```bash
sudo tee /root/nginx_hc.sh <<-EOF
#!/bin/sh
curl -f http://localhost/hostname
EOF
chmod +x /root/nginx_hc.sh
kill -1 $(ps -ef | grep keepalived | grep -v grep|tail -1 | awk '{print $2}')
```

Restart keepalived and note the master node

sudo service nginx stop on the master VM and observe the other VM become the master
```bash
sudo service nginx stop 
```
Putting it all together
Now, set up both Nginx with proxy_pass to the third VM where you deployed the example "shopping cart" web app.


#### Server A and B
```bash
sudo tee /etc/nginx/sites-available/lab <<-EOF
```
```nginx
server {
    location /hostname {
        default_type text/html;
        return 200 \$hostname;
        proxy_pass http://192.168.100.124;
    }
    location /hostname2 {
        proxy_pass http://192.168.100.124;
    }
}
```
```bash
EOF
```
```bash
sudo nginx -t && sudo service nginx reload
```

#### Server C
```bash
sudo tee /etc/nginx/sites-available/lab <<-EOF
```
```nginx
server {
    location /hostname2 {
        default_type text/html;
        return 200 \$hostname;
    }
}
```
```bash
EOF
```


```bash
cd /etc/nginx/sites-enabled/
    # unlink default
sudo nginx -t && sudo service nginx reload
```


    As you can imagine, you can have multiple VMs in the upstream group on the HA servers, eliminating a single point of failure.

    As an extension, team up with a colleague and put your VMs together for both HA and load balancing.

    Let each of the HA nodes add a header with its hostname (i.e. add_header X-Load-Balancer-Node $hostname;) and a header with upstream IP (add_header X-Upstream $upstream_addr)

    Observe how X-Upstream will change according to the load-balancing strategy (round-robin by default), and how X-Load-Balancer-Node will reflect the master node of the HA cluster.




#### Server RAED A 116

```bash
sudo tee /root/keepalived.conf <<-EOF
```
```json
global_defs {
    vrrp_version 3
}


vrrp_script nginx_hc {
  script       "/root/nginx_hc.sh"
  interval 2   # check every 2 seconds
}


vrrp_instance VI_1 {
    interface                  ens192
    priority                   101
    advert_int                 1
    accept
    virtual_router_id          51

    unicast_peer {
        192.168.100.117
    }

    virtual_ipaddress {
        51.79.106.247 dev ens160
    }


        notify_master "/bin/bash -c '(route add 54.38.17.254 dev ens160 && echo Wait && route -n &&  sleep 5 && route -n && route add default gw 54.38.17.254) > /tmp/ok 2>&1'"


    track_script {
        nginx_hc
    }
}
```
```bash
EOF
```



#### Server RAED A 117

```bash
sudo tee /root/keepalived.conf <<-EOF
```
```json
global_defs {
    vrrp_version 3
}


vrrp_script nginx_hc {
  script       "/root/nginx_hc.sh"
  interval 2   # check every 2 seconds
}


vrrp_instance VI_1 {
    interface                  ens192
    priority                   101
    advert_int                 1
    accept
    virtual_router_id          51

    unicast_peer {
        192.168.100.116
    }

    virtual_ipaddress {
        51.79.106.247 dev ens160
    }


        notify_master "/bin/bash -c '(route add 54.38.17.254 dev ens160 && echo Wait && route -n &&  sleep 5 && route -n && route add default gw 54.38.17.254) > /tmp/ok 2>&1'"


    track_script {
        nginx_hc
    }
}
```
```bash
EOF
```


#### Server RAF A 122

```bash
sudo tee /root/keepalived.conf <<-EOF
```
```json
global_defs {
    vrrp_version 3
}


vrrp_script nginx_hc {
  script       "/root/nginx_hc.sh"
  interval 2   # check every 2 seconds
}


vrrp_instance VI_1 {
    interface                  ens192
    priority                   101
    advert_int                 1
    accept
    virtual_router_id          51

    unicast_peer {
        192.168.100.123
        192.168.100.124
    }

    virtual_ipaddress {
        51.79.106.247 dev ens160
    }


        notify_master "/bin/bash -c '(route add 54.38.17.254 dev ens160 && echo Wait && route -n &&  sleep 5 && route -n && route add default gw 54.38.17.254) > /tmp/ok 2>&1'"


    track_script {
        nginx_hc
    }
}
```
```bash
EOF
```


####S erver RAF B 123
```bash
sudo tee /root/keepalived.conf <<-EOF
```
```json
global_defs {
    vrrp_version 3
}


vrrp_script nginx_hc {
  script       "/root/nginx_hc.sh"
  interval 2   # check every 2 seconds
}


vrrp_instance VI_1 {
    interface                  ens192
    priority                   101
    advert_int                 1
    accept
    virtual_router_id          51

    unicast_peer {
        192.168.100.116
        192.168.100.117
        192.168.100.123
    }

    virtual_ipaddress {
        51.79.106.247 dev ens160
    }


        notify_master "/bin/bash -c '(route add 54.38.17.254 dev ens160 && echo Wait && route -n &&  sleep 5 && route -n && route add default gw 54.38.17.254) > /tmp/ok 2>&1'"


    track_script {
        nginx_hc
    }
}
```
```bash
EOF
```


#### Server RAF A 124

```bash
sudo ifconfig ens160 up

sudo tee /root/keepalived.conf <<-EOF
```
```json
global_defs {
    vrrp_version 3
}


vrrp_script nginx_hc {
  script       "/root/nginx_hc.sh"
  interval 2   # check every 2 seconds
}


vrrp_instance VI_1 {
    interface                  ens192
    priority                   101
    advert_int                 1
    accept
    virtual_router_id          51

    unicast_peer {
        192.168.100.122
        192.168.100.123
    }

    virtual_ipaddress {
        51.79.106.247 dev ens160
    }


        notify_master "/bin/bash -c '(route add 54.38.17.254 dev ens160 && echo Wait && route -n &&  sleep 5 && route -n && route add default gw 54.38.17.254) > /tmp/ok 2>&1'"


    track_script {
        nginx_hc
    }
}
```
```bash
EOF
```



#### Servers A and B and C RAF
```bash
sudo tee /etc/nginx/sites-available/lab <<-EOF
```
```nginx
upstream backendlab {
        server 192.168.100.118;
        server 192.168.100.124;

}
server {
    location /hostname {
        default_type text/html;
        return 200 \$hostname;
    }
    location /hostname2 {
        proxy_pass http://backendlab;
    }
}
```
```bash
EOF
```

```bash
sudo nginx -t && sudo service nginx reload


sudo tee /etc/nginx/sites-available/lab <<-EOF
```
```nginx
upstream backendlab {
        server 192.168.100.116;
        server 192.168.100.117;
        server 192.168.100.118;

}
server {
    location /hostname {
        default_type text/html;
        return 200 \$hostname;
    }
    location /hostname2 {
        proxy_pass http://backendlab;
    }
}
```
```bash
EOF
```
```bash
sudo nginx -t && sudo service nginx reload
```

#### Servers A b C Raed

```bash
sudo tee /etc/nginx/sites-available/lab <<-EOF
```
```nginx
server {
    location /hostname2 {
        default_type text/html;
        return 200 \$hostname;
    }
}
```
```bash
EOF
```
```bash
sudo nginx -t && sudo service nginx reload

sudo tee /etc/nginx/sites-available/lab <<-EOF
```
```nginx
server {
    location /hostname2 {
        default_type text/html;
        return 200 \$hostname;
    }
}
```
```bash
EOF
```
```bash
sudo nginx -t && sudo service nginx reload
```



