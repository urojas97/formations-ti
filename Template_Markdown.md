# Ansible
## Les liens

Vidéos Youtube [Playlist by Yogesh Metha](https://www.youtube.com/playlist?list=PLLsor6GJ_BEEC9jUSc760iqaOx6u5lqRA)

Example de graphs [Graph](https://graphs.grevian.org/example)

Generateur de graphs [Graph](http://www.webgraphviz.com/)

-----------------------

![Ansible](https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Ansible_logo.svg/256px-Ansible_logo.svg.png)

-----------------------

Specifier le type de code
```bash
for i in [1,2,3]; do echo $i; done
```

Il y a une plétore de code prise en compte, Dockerfile, nginx, json,

Voici un lien parfait:
https://highlightjs.org/static/demo/



---------------------
Ansible introduction  
Automation engine -> provision, config, deployment, orchestration, ...

Language: Python  
Requirement: OpenSSH, Python, Jinja2  
Notion de module 

Repository: EPEL

---------------------

Mot en italique *italique* 

Mot en  gras **gras**

## Les listes à puces

### Liste simple
* Une puce
* Une autre puce
* Et encore une autre puce !

### Liste numérotés
1. Et de un
2. Et de deux
3. Et de trois
-----------------------

## Les citations
> citation 1

paragraphe

> citation 2

paragraphe

----------------------
## Les Graphs

Liens cliquables

```dot
digraph notebook {
  exp1 [label="experiment 1",URL="http://example.com/1"]
  exp2 [label="experiment 2",URL="http://example.com/2"]
  exp3 [label="experiment 3",URL="http://example.com/3"]
  exp1 -> exp3;
  exp2 -> exp3;
}
```

```dot
digraph {
  tbl [
    shape=plaintext
    label=<
      <table border='0' cellborder='1' color='blue' cellspacing='0'>
        <tr><td>foo</td><td>bar</td><td>baz</td></tr>
        <tr><td cellpadding='4'>
          <table color='orange' cellspacing='0'>
            <tr><td>one  </td><td>two  </td><td>three</td></tr>
            <tr><td>four </td><td>five </td><td>six  </td></tr>
            <tr><td>seven</td><td>eight</td><td>nine </td></tr>
          </table>
        </td>
        <td colspan='2' rowspan='2'>
          <table color='pink' border='0' cellborder='1' cellpadding='10' cellspacing='0'>
            <tr><td>eins</td><td>zwei</td><td rowspan='2'>drei<br/>sechs</td></tr>
            <tr><td>vier</td><td>fünf</td>                             </tr>
          </table>
        </td> 
        </tr>
        <tr><td>abc</td></tr>
      </table>
    >];
}
```

```dot
digraph D {

  node [shape=plaintext]

  some_node [
   label=<
     <table border="0" cellborder="1" cellspacing="0">
       <tr><td bgcolor="yellow">Foo</td></tr>
       <tr><td bgcolor="lightblue"><font color="#0000ff">Bar</font></td></tr>
       <tr><td bgcolor="#f0e3ff"><font color="#ff1020">Baz</font></td></tr>
     </table>>
  ];
}
```


```dot
digraph notebook {
  "Gluu Actif" -> "Slapcat Online";
  "Gluu Backup" -> "Gluu Actif"[color=red,label=<<font color='red'>CacheRefresh</font>>,weight="0.1"];
  "Gluu Backup" -> "Slapcat Offline";
}
```



```dot
digraph notebook {
  exp1 -> exp3;
  exp2 -> exp3;
}
```

```dot
digraph Test{
    a -> b[label="0.2",weight="0.2"];
    a -> c[label="0.4",weight="0.4"];
    c -> b[label="0.6",weight="0.6"];
    c -> e[label="0.6",weight="0.6"];
    e -> e[label="0.1",weight="0.1"];
    e -> b[label="0.7",weight="0.7"];
}
```
```dot
graph Titi{
    splines=line;
    subgraph cluster_0 {
        label="Subgraph A";
        a; b; c
    }

    subgraph cluster_1 {
        label="Subgraph B";
        d; e;
    }

    a -- e;
    a -- d;
    b -- d;
    b -- e;
    c -- d;
    c -- e;
}
```

```dot
digraph Toto{
    subgraph cluster_0 {
        label="Subgraph A";
        a -> b;
        b -> c;
        c -> d;
    }

    subgraph cluster_1 {
        label="Subgraph B";
        a -> f;
        f -> c;
    }
}
```
```dot
graph {
    a -- b[color=red,penwidth=3.0];
    b -- c;
    c -- d[color=red,penwidth=3.0];
    d -- e;
    e -- f;
    a -- d;
    b -- d[color=red,penwidth=3.0];
    c -- f[color=red,penwidth=3.0];
}
```

```dot
digraph GluuPrd{
    rankdir=LR;
    color=cyan;
    subgraph cluster_0 {
        label="Prod"
        node [style=filled,color=cyan];
        prdlgla01a [shape=Square] ;
        node [style=filled,color=cyan]
        prdlgla01a -> "Crontab" -> Container -> "AllDay@1am" -> OnlineBackup;
        "Crontab"  -> OS -> "AllDay@3am" -> RemoveBackup;
    }
    subgraph cluster_1 {
        color=green;
        label="Backup"
        node [style=filled, color=green];
        "prdlglb01a\n(Backup)" [shape=Square];
        node [style=filled,color=green]
        "prdlglb01a\n(Backup)"-> "_Crontab" -> _Container -> "_AllDay@1am" -> StopSolserver -> _BackupSlapcat -> StartSolserver;
        "_Crontab" -> _OS -> "_AllDay@3am" -> _RemoveBackup;
    }
    prdlgla01a -> "prdlglb01a\n(Backup)"[color=red,label=<<font color='red'>Cache Refresh</font>>,weight="0.1"]
}
```
---

## Afficher du code

### En Bloc

Voici un code en C :

    int main()
    {
        printf("Hello world!\n");
        return 0;
    }

### En ligne
La fonction `printf()` permet d'afficher du texte

------------------------

## Tableau

| Tables   |      Are      |  Cool |
|----------|:-------------:|------:|
| col 1 is |  left-aligned | $1600 |
| col 2 is |    centered   |   $12 |
| col 3 is | right-aligned |    $1 |

[Link to table generator](https://www.tablesgenerator.com/markdown_tables)


---------------------
Conclusion

-----------------------
-----------------------






